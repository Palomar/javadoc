package Cartes;

/**
 * La classe Carta emmagatzema en una variable tipus String
 * el tipus de carta i en un enter el nombre de la carta.
 * <p>
 * Diposa de m�todes que permeten obtenir i fixar els seus
 * atributs, sobrescriure el m�tode de presentaci� {@link #toString()},
 * comprovaci� del nombre i el tipus de carta correcte.
 * Entre els seus atributs es troba el tipus static de la cadena
 * de text amb els tipus de cartes.
 * 
 * @version 1
 * @author ies
 *
 */
public class Carta {
    protected String tipus;
    protected int nombre;

    static protected String [] TIPUS = {"Ors", "Bastos", "Espases", "Copes"};

    /**
     * Constructor de l'objecte Carta amb el tipus i el nombre
     * enviats per par�metre.
     * <p>
     * Abans d'assignar els valors enviats per par�metre per
     * crear l'objecte Carta, es fa una comprobaci� del tipus de carta
     * cridant {@link #IsTipusOK(String)} i si es true l'assigna, si no ens
     * donar� error. El mateix amb la comprovaci� del nombre amb
     * el m�tode {@link #IsNombreOK(int)} si retorna un true s'assigna i
     * en cas contrari s'assigna -1.
     * 
     * @param tipus tipus cadena de text, on figura el tipus de carta
     * @param nombre tipus enter, on figura el nombre que li correspon a la carta
     */
    Carta(String tipus, int nombre)
    {
        if (IsTipusOK(tipus)) {
            this.tipus = tipus;
        }
        else {
            this.tipus = "ERROR";
        }

        if (IsNombreOK(nombre)) {
            this.nombre = nombre;
        }
        else {
            this.nombre = -1;
        }
    }

    /**
     * M�tode que comprova que el nombre enter sigui v�lid
     * per a crear l'objecte Carta.
     * <p>
     * Es fa una comprovaci� per saber si est� entre 1 i 12,
     * si ho est� retorna true, si no es false.
     * 
     * @param nombre tipus enter, nombre que es vol comprovar
     * @return retorna un boole� per saber si �s v�lid
     */
    public boolean IsNombreOK(int nombre) {
        if (nombre > 0 && nombre < 13) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * M�tode que comprova que la cadena de text del tipus
     * de carta sigui v�lida.
     * <p>
     * Fa una comprovaci� directament amb l'array static de
     * tipus de cartes que hi pot haver. En cas de ser v�lid
     * retorna true, si no �s false.
     * 
     * @param tipus cadena de text amb el tipus que volem comprovar
     * @return retorna boole� segons si �s v�lid o no el tipus de carta
     */
    public boolean IsTipusOK(String tipus) {
        boolean tipus_ok = false;

        for (int i = 0; i < TIPUS.length; i++) {
            if (tipus.equals(TIPUS[i])) {
                tipus_ok = true;
            }
        }

        return tipus_ok;
    }

    /**
     * M�tode que retorna de quin tipus de carta es tracta l'objecte.
     * 
     * @return retorna una cadena de text del tipus de carta
     */
    public String getTipus() {
        return tipus;
    }

    /**
     * M�tode que introdueix el tipus de carta que enviem per
     * par�metre.
     * <p>
     * Fa una comprovaci� per saber si el tipus de carta �s v�lida
     * amb el m�tode {@link #IsTipusOK(String)} i fixa el tipus si
     * �s v�lid.
     * 
     * @param tipus cadena de text amb el tipus de carta
     */
    public void setTipus(String tipus) {
        if (IsTipusOK(tipus)) {
            this.tipus = tipus;
        }
    }

    /**
     * M�tode que retorna el nombre de la carta de l'objecte.
     * 
     * @return retorna un enter amb el nombre de la carta.
     */
    public int getNombre() {
        return nombre;
    }

    /**
     * M�tode que introdueix el nombre que enviem per
     * par�metre.
     * <p>
     * Fa una comprovaci� per saber si el nombre de la carta �s v�lid
     * amb el m�tode {@link #IsNombreOK(int)} i fixa el nombre si
     * �s v�lid.
     * 
     * @param nombre tipus enter, �s el nombre que volem introduir a la carta
     */
    public void setNombre(int nombre) {
        if (IsNombreOK(nombre)) {
            this.nombre = nombre;
        }
    }

    /**
     * M�tode que sobrescriu el m�tode {@link #toString()}  de
     * l'objecte per produir una sortida amb un format de la
     * cadena de text.
     * 
     * @return cadena de text amb el format de presentaci� de l'objecte carta
     * 
     */
    public String toString() {
        return tipus + " " + nombre;
    }
}


package Cartes;

/**
 * La classe Baralla emmagatzema una array de tipus Carta
 * a la variable "cartes" i una array de cadenes de text de tipus static
 * amb els valors de tipus de cartes.
 * <p>
 * Diposa d'un m�tode per crear la baralla amb el seu constructor.
 * 
 * @version 1
 * @author ies
 *
 */
public class Baralla {
    protected Carta [] cartes;
    static protected String [] TIPUS = {"Ors", "Bastos", "Espases", "Copes"};

    /**
     * Constructor de l'objecte Baralla omplin l'array de tipus Carta
     * amb totes les cartes de la baralla.
     * <p>
     * Despr�s de crear-la, fa una impressi� de les cartes dintre
     * d'un cicle amb el m�tode {@link Carta#toString()}
     * 
     */
    public Baralla() {
        cartes = new Carta[48];
        int k=0;
        for (int i=0; i<TIPUS.length; i++) {
            for (int j=1; j<=12; j++) {
                cartes[k++] = new Carta(TIPUS[i], j);
            }
        }

        for (int i=0; i<cartes.length; i++) {
            System.out.println(cartes[i].toString());
        }
    }
}

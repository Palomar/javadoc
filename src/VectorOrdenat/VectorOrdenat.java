package VectorOrdenat;

/**
 * Aquesta classe ens permet reordenar un array de n�meros.
 * <p>
 * l'array s'emmagatzema en una variable "vector" de tipus
 * double[]. Diposa de m�todes per inserir un nombre, per
 * cercar un nombre, per unir, per ordenar, per calcular la seva
 * mida, per recuperar l'array, per recuperar el valor de l'�ndex de
 * l'array que volem, per extreure de l'array un nombre.
 * 
 * @version 1
 * @author ies
 *
 */
public class VectorOrdenat {
    private double[] vector;

    /**
     * Constructor que inicialitza a null la variable
     * array "vector".
     */
    public VectorOrdenat() {
        vector = null;
    }

    /**
     * M�tode que permet inserir el nombre double que pasem per par�metre
     * a l'array "vector".
     * <p>
     * Primer es prepara una array auxiliar amb una posici� d'�ndex extra
     * que permet inserir el nou nombre. Es fa una comprovaci� en un cicle
     * que permet inserir-lo si es igual o m�s petit segons la posici� de
     * l'�ndex del vector original, per despr�s moure una posici� a l'auxiliar
     * i assignar el seg�ent del vector original.
     * Una vegada s'ha inserit, es fa una copia del nou vector auxiliar a 
     * l'array de la variable vector original.
     * 
     * @param n tipus double, nombre que volem inserir dintre de l'array
     */
    public void insereix(double n) {
        int len = vector.length;
        double[] aux = new double[len + 1];
        int j=0;
        boolean no_insertat = true;
        for (int i = 0; i < len; i++, j++) {
            if (vector[i] >= n && no_insertat) {
                aux[j] = n;
                j++;
                no_insertat = false;
            }
            aux[j] = vector[i];
        }

        // Copiem el nou vector amb el valor inserit
        vector = new double[len + 1];
        for (int i=0; i<len+1; i++) {
            vector[i] = aux[i];
        }
    }

    /**
     * M�tode que retorna l'�ndex de la posici� on es troba el
     * nombre que pasem per par�metre.
     * 
     * @param n tipus double, nombre que busquem a l'array
     * @return retorna un enter de la posici�n �ndex de l'array
     */
    public int cerca(double n) {
        int pos = -1;
        for (int i=vector.length-1; i>=0; i--) {
            if (vector[i] == n) {
                pos = i;
            }
        }
        return pos;
    }

    /**
     * M�tode que serveix per unir dos arrays de doubles.
     * <p>
     * Es calcula la longitud m�xima entre els dos arrays,
     * es crea una array auxiliar on s'emmagatzema l'array
     * de l'objecte i posteriorment es contin�a fent la uni�
     * de l'array passada per par�metre.
     * Finalment es fa una copia de l'array auxiliar al vector
     * original.
     * 
     * @param unir tipus array double, array de nombres que volem unir a l'objecte
     */
    public void uneix(double[] unir) {
        int len = vector.length + unir.length;
        double aux[] = new double[len];

        for (int i=0; i<vector.length; i++) {
            aux[i] = vector[i];
        }

        for (int i=0; i<unir.length; i++) {
            aux[vector.length+i] = unir[i];
        }

        vector = new double[len];
        for (int i=0; i<aux.length; i++) {
            vector[i] = aux[i];
        }

    }

    /**
     * M�tode que s'encarrega d'ordenar de menor a major
     * els nombres de l'array.
     * <p>
     * Fent servir una variable boolean y una variable auxiliar
     * per emmagatzemar el nombre que mourem de posici�,
     * s'aplica un cicle que mentre hi hagi un canvi, faci una
     * iteraci� m�s sobre un bucle que comprova que tota l'array
     * est� ordenada, i de no estar-ho, procedeix a fer l'intercanvi
     * per mitja de l'ordenament de bombolla.
     */
    public void ordena() {
        boolean canvi = true;
        double aux = 0;

        // Bombolla
        while (canvi) {
            canvi = false;
            for (int i=0; i<vector.length-1; i++) {
                if (vector[i] > vector[i+1]) {
                    canvi = true;
                    aux = vector[i];
                    vector[i] = vector[i+1];
                    vector[i+1] = aux;
                }
            }
        }
    }

    /**
     * M�tode que retorna la mida de l'array.
     * 
     * @return tipus enter, retorna la mida de l'array
     */
    public int mida() {
        return vector.length;
    }

    /**
     * M�tode que fa una c�pia de l'array de nombres a
     * una variable auxiliar que la retorna.
     * 
     * @return tipus array double, retorna el vector de nombres
     */
    public double[] getVector() {
        double[] aux;
        aux = new double[vector.length];

        for (int i=0; i<vector.length; i++) {
            aux[i] = vector[i];
        }

        return aux;
    }

    /**
     * M�tode que retorna el valor de l'�ndex de l'array 
     * proporcionat per par�metre.
     * 
     * @param index tipus enter, la posici� del valor de l'array
     * @return retorna un tipus double amb el valor de la posici� de l'array
     */
    public double getIndex(int index) {
        double valor = 0;

        if (index >= 0 && index < vector.length) {
            valor =  vector[index];
        }
        else {
            System.err.println("Index incorrecte!");
        }

        return valor;
    }

    /**
     * M�tode que extreu el nombre double proporcionat per par�metre
     * de l'array vector.
     * <p>
     * Primer es busca l'�ndex de posici� de l'array d'on es troba
     * el nombre enviat per par�metre. Posteriorment si en troba,
     * es fa una array auxiliar i a partir d'aquesta es fa una copia
     * de l'original fins la posici� i posteriorment es copia la resta segons la extracci�
     * feta.
     * 
     * @param n tipus double, el nombre que volem extreure de l'array
     */
    public void extreu(double n) {
        double[] aux;
        aux = new double[vector.length-1];
        int pos=-1;

        boolean trobat = false;

        for (int i=0; i<vector.length; i++) {
            if (vector[i] == n) {
                trobat = true;
                pos = i;
            }
        }

        if (trobat) {
            for (int i=0; i<pos; i++) {
                aux[i] = vector[i];
            }
            for (int i=pos+1; i<vector.length; i++) {
                aux[i-1] = vector[i];
            }
        }
    }
}









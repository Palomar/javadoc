package JavadocPoligon;

/**
 * La classe Poligon construeix un poligon a base de Segments.
 * <p>
 * S'emmagatzema en una array de tipus Segment les diferents
 * parts de segments que componen l'objecte Poligon.
 * Es guarda en una variable la quantitat de segments del Poligon,
 * i per altra banda el n�mero de segments que s'han incl�s dintre
 * de l'objecte Poligon.
 * 
 * @version 1
 * @author ies
 *
 */

public class Poligon {
    protected Segment [] segments;
    protected int num_segments;
    protected int max_segments;
/**
 * Constructor de la classe Poligon que rep el n�mero de segments
 * totals que ha de tenir.
 * <p>
 * Per mitj� de la classe Segment es crea la array amb el m�xim
 * de segments que li hem proporcionat i la emmagatzemem a la variable
 * segments que es una array.
 * 
 * @param num_segments tipus enter, quantitat m�xima de segments
 */
    public Poligon(int num_segments) {
        max_segments = num_segments;
        segments = new Segment [max_segments];
    }
    
    /** M�tode que serveix per afegir un Segment enviat per par�metre
     * a l'objecte Poligon.
     * <p>
     * Cada cop que afegim un Segment a la array de segments,
     * s'incrementa el valor del nombre de segments que hem
     * afegit. I mai s'afegir� m�s segments si hem arribat
     * al nombre m�xim perm�s.
     *
     * 
     * @param s tipus Segment, segment que s'afegeix al Poligon
     */

    public void addSegment(Segment s) {
        if (num_segments < max_segments) {
            segments[num_segments] = new Segment(s);
            num_segments++;
        }
    }
/**
 * M�tode que afegeix un Segment per mitj� de les coordenades
 * x i y inicials i finals, enviades per par�metre.
 * 
 * @param x0 tipus enter, coordenada x inicial
 * @param y0 tipus enter, coordenada y inicial
 * @param x1 tipus enter, coordenada x final
 * @param y1 tipus enter, coordenada y final
 */
    public void addSegment(int x0, int y0, int x1, int y1) {
        /*
        Punt p0 = new Punt(x0, y0);
        Punt p1 = new Punt(x1, y1);
        Segment s = new Segment(p0, p1);
        */

        Segment s = new Segment(x0, y0, x1, y1);
        addSegment(s);
    }
   
    /**
     * M�tode que sobrescriu el m�tode {@link #toString()} per fer una presentaci�
     * diferent del Poligon.
     * 
     * @return retorna una cadena de text amb les dades de Segments
     */
    public String toString() {
        String str = new String("");
        for (int i=0; i<num_segments; i++) {
            str += segments[i].toString() + "\n";
        }
        return str;
    }
}

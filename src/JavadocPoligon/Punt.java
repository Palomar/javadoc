package JavadocPoligon;

/**
 * La classe Punt gestiona objectes de tipus Punt amb la seva
 * coordenada x i la coordenada y.
 * <p>
 * Amb dos nombres tipus enter es guarden les seves coordenades.
 * 
 * @version 1
 * @author ies
 *
 */
public class Punt {
    protected int x;
    protected int y;
/**
 * Constructor de la classe Punt que crea el Punt a partir de
 * les coordenades x i y.
 * <p>
 * S'assignen les variables per par�metre a les de l'objecte Punt.
 *  
 * @param x tipus enter, coordenada x del Punt
 * @param y tipus enter, coordenada y del Punt
 */
    public Punt(int x, int y) {
        this.x = x;
        this.y = y;
    }
/**
 * Constructor de la classe Punt que crea el Punt a partir
 * d'un altre Punt per par�metre.
 * <p>
 * Fent servir el m�tode {@link Punt#getX()} i {@link Punt#getY()} del Punt
 * enviar per par�metre, es crea un Punt assignant la coordenada x i y.
 * 
 * @param p tipus Punt, fet servir per crear un altre Punt
 */
    public Punt(Punt p) {
        x = p.getX();
        y = p.getY();
    }
    
    /**
     * M�tode que sobrescriu el m�tode {@link #toString()} per fer una presentaci�
     * diferent del Punt.
     * 
     * @return retorna una cadena de text amb les dades del Punt 
     */

    public String toString() {
        return String.format("(%d, %d)", x, y);
    }
/**
 * M�tode que retorna la coordenada x del Punt.
 * 
 * 
 * @return retorna el tipus enter de la coordenada x
 */
    public int getX() {
        return x;
    }
    
    /**
     * M�tode que retorna la coordenada y del Punt.
     * 
     * 
     * @return retorna el tipus enter de la coordenada y
     */
    public int getY() {
        return y;
    }
/**
 * M�tode que actualitza la coordenada x del Punt.
 * 
 * @param x tipus enter, coordenada x enviada
 */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * M�tode que actualitza la coordenada y del Punt.
     * 
     * @param y tipus enter, coordenada y enviada
     */
    public void setY(int y) {
        this.y = y;
    }
}

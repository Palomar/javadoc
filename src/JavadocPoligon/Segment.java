package JavadocPoligon;

/**
 * La classe Segment gestiona segments amb dos punts de la classe Punt.
 * <p>
 * Fent servir dos variables de classe Punt p0 i p1 es guarden les coordenades
 * de disposicio inicial i final del Segment.
 * 
 * @version 1
 * @author ies
 *
 */
public class Segment {
    protected Punt p0;
    protected Punt p1;

    /**
     * Constructor que crea el Segment fent servir un altre Segment.
     * <p>
     * Fixa els valors p0 i p1 a partir dels punts del segment enviat per
     * par�metre.
     * 
     * @param s	tipus Segment que serveix per crear un altre Segment
     * @see Punt
     *
     */
    
    public Segment(Segment s) {
        p0 = new Punt(s.getPunt0());
        p1 = new Punt(s.getPunt1());
    }

    /**
     * Constructor que crea el Segment fent servir dos Punts.
     * <p>
     * Fixa els valors de p0 i p1 a partir dels punts enviats per
     * par�metre.
     * 
     * @param p0 tipus Punt que serveix per crear un altre Punt inicial
     * @param p1 tipus Punt que serveix per crear un altre Punt final
     * @see Punt
     */
    public Segment(Punt p0, Punt p1) {
        this.p0 = new Punt(p0);
        this.p1 = new Punt(p1);
    }
    
    /**
     * Constructor que crea el Segment fent servir 4 coordenades de Punts.
     * <p>
     * Fixa els valors de p0 i p1 a partir de les coordenades x0, y0, x1, y1
     * enviades per par�metre. A partir d'aquestes es creen el 2 Punts que
     * calen emmagatzemar-se a p0 i p1.
     * 
     * @param x0 tipus enter, coordenada x del punt inicial
     * @param y0 tipus enter, coordenada y del punt inicial
     * @param x1 tipus enter, coordenada x del punt final
     * @param y1 tipus enter, coordenada y del punt final
     * @see Punt
     */

    public Segment(int x0, int y0, int x1, int y1) {
        this.p0 = new Punt(x0, y0);
        this.p1 = new Punt(x1, y1);
    }

    /**
     * M�tode que serveix per recuperar el punt inicial del Segment.
     * 
     * @return retorna el punt inicial del segment tipus Punt.
     */
    public Punt getPunt0() {
        return p0;
    }

    /**
     * M�tode que serveix per recuperar el punt final del Segment.
     * 
     * @return retorna el punt final del segment tipus Punt.
     */
    public Punt getPunt1() {
        return p1;
    }

    /**
     * M�tode que sobrescriu el m�tode {@link #toString()} per fer una presentaci�
     * diferent del Segment.
     * 
     *  @return retorna una cadena de text amb les dades del Segment
     */
    public String toString() {
        String str;
        str = "(" + p0.toString() + p1.toString() + ")";
        return str;
    }
}

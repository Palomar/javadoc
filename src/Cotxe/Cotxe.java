package Cotxe;

/**
 * La classe Cotxe crea objectes amb tres cadenes de text on figuren
 * la seva matr�cula, el color i un registre per saber quina ser�
 * la seg�ent matr�cula.
 * <p>
 * Disposa de m�todes per crear la matr�cula generant-la a partir del
 * registre static emmgatzemat a l'objecte, per recuperar el color i la matr�cula,
 * a m�s de dos tipus de constructor.
 * 
 * @version 1
 * @author ies
 *
 */
public class Cotxe {
    static String seguentMatricula = "0000-AAA";
    private String matricula;
    private String color;

    /**
     *  Constructor de la classe Cotxe per defecte sense par�metres.
     *  <p>
     *  Nom�s indica un missatge i figura el color gris.
     */
    public Cotxe() {
        this("gris");
        System.out.println("Constructor sense par�metres");
    }

    /**
     * Constructor alternatiu de la classe Cotxe que accepta un par�metre.
     * <p>
     * Es crea l'objecte Cotxe a partir del par�metre de color i es fa una
     * crida al m�tode {@link #creaMatricula()} per assignar la matr�cula.
     * 
     * @param color cadena de text on s'especifica el color del cotxe
     * @see #creaMatricula()
     */
    public Cotxe(String color) {
        System.out.println("Constructor que rep un color");
        this.color = color;
        matricula = creaMatricula();
    }

    /**
     * M�tode per assignar matr�cula a l'objecte Cotxe.
     * <p>
     * Primer s'agafa el valor de variable static per saber per quin
     * registre de matr�cula li toca. Extreu el s�mbol "-" per processar
     * els n�meros per tal d'operar la nova assignaci� num�rica.
     * Posteriorment treballa amb l'assignaci� de la nova lletra si cal.
     * 
     * @return retorna una cadena de text de la matr�cula assignada
     */
    private String creaMatricula() {
        String novaMatricula = seguentMatricula;
        String[] m = seguentMatricula.split("-");
        int num = Integer.parseInt(m[0]);
        if (num < 9999) {
            num++;
            seguentMatricula = String.format("%04d-%s", num, m[1]);
        } else {
			/* Per simplificar s'ha tret aquesta part del codi,
			 * que hauria d'augmentar les lletres de la matrícula cada cop
			 * que s'arriba a 9999 al nombre.
			 */
        }
        return novaMatricula;
    }

    /**
     * M�tode que recupera el color del cotxe.
     * 
     * @return retorna la cadena de text del color
     */
    public String getColor() {
        return color;
    }

    /**
     * M�tode que recupera el n�mero de matr�cula del cotxe
     * 
     * @return retorna la cadena de alfanum�rica de la matr�cula
     */
    public String getMatricula() {
        return matricula;
    }
}
package Cotxe;

public class Main {
    public static void main(String[] args) {
        Cotxe c;
        String[] colors = {"blanc", "vermell"};
        for (int i=0; i<2; i++) {
            c = new Cotxe(colors[i]);
            descriuCotxe(c);
        }
        c = new Cotxe();
        descriuCotxe(c);
    }
    public static void descriuCotxe(Cotxe c) {
        System.out.println("Matrícula: "+c.getMatricula()+"  Color: "+c.getColor());
    }
}